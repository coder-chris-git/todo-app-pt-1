import TodoItem from "../todo-item/TodoItem";
import React from "react";

const TodoList = (props) => {
  return (
    <section className="main">
      <ul className="todo-list">
        {props.todos.map((todo) => (
          <TodoItem
            key={todo.id}
            title={todo.title}
            completed={todo.completed}
            id={todo.id}
            handleToggle={props.handleToggle}
            handleDelete={props.handleDelete}
          />
        ))}
      </ul>
    </section>
  );
};

export default TodoList;
