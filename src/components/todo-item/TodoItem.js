import React from "react";

const TodoItem = (props) => {
  return (
    <li className={props.completed ? "completed" : ""}>
      <div className="view">
        <input
          className="toggle"
          type="checkbox"
          checked={props.completed}
          onChange={() => props.handleToggle(props.id)}
        />
        <label>{props.title}</label>
        <button onClick={(event) => props.handleDelete(props.id)} className="destroy" />
      </div>
    </li>
  );
};

export default TodoItem;
