import React from "react";
import TodoList from "./components/todo-list/TodoList";
import { useState } from "react";
import { v1 as uuidv1 } from "uuid";
import { todos as todosList } from "./todos";
const App = () => {
  const [todos, setTodos] = useState(todosList);
  const [inputText, setInputText] = useState("");

  const Add = (event) => {
    if (event.key === "Enter") {
      const ids = uuidv1();

      const newTodo = {
        userId: 1,
        id: ids,
        title: inputText,
        completed: false,
      };

      const newTodos = {
        ...todos,
      };
      newTodos[ids] = newTodo;
      setTodos(newTodos);

      setInputText("");
    }
  };
  const handleToggle = (id) => {
    const newTodos = { ...todos };
    newTodos[id].completed = !newTodos[id].completed;
    setTodos(newTodos);
  };
  const handleDelete = (id) => {
    const newTodos = { ...todos };
    delete newTodos[id];
    setTodos(newTodos);
  };

  const deleteAll = () => {
    const newTodos = { ...todos };
    console.log(newTodos);
    for (const todo in newTodos) {
      if (newTodos[todo].completed) {
        delete newTodos[todo];
      }
    }
    setTodos(newTodos);
  };

  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <input
          onKeyDown={(event) => Add(event)}
          onChange={(event) => setInputText(event.target.value)}
          value={inputText}
          className="new-todo"
          placeholder="What needs to be done?"
          autoFocus
        />
      </header>

      <TodoList todos={Object.values(todos)} handleToggle={handleToggle} handleDelete={handleDelete} />
      <footer className="footer">
        <span className="todo-count">
          <strong>{}</strong> item(s) left
        </span>
        <button onClick={(event) => deleteAll()} className="clear-completed">
          Clear completed
        </button>
      </footer>
    </section>
  );
};

export default App;
